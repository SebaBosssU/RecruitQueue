require([
    'two/ready',
    'two/recruiter',
    'two/recruiter/ui'
], function (
    ready,
    RecruitQueue
) {
    if (RecruitQueue.initialized) {
        return false
    }

    ready(function () {
        RecruitQueue.init()
        RecruitQueue.interface()
    })
})
