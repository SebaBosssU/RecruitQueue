define('two/recruiter', [
    'two/locale',
    'two/eventQueue',
    'Lockr'
], function (
    Locale,
    eventQueue,
    Lockr
) {
    var initialized = false
    var running = false
    var player
    var RecruitQueue = {}
    RecruitQueue.version = '__recruiter_version'
    RecruitQueue.init = function init() {
        Locale.create('recruiter', __recruiter_locale, 'pl')

        RecruitQueue.initialized = true
        player = modelDataService.getSelectedCharacter()
        groupList = modelDataService.getGroupList()
    }
    RecruitQueue.start = function (disableNotif) {
        running = true
        eventQueue.trigger('RecruitQueue/start', [disableNotif])
    }
    RecruitQueue.start2 = function (disableNotif) {
        running = true
        eventQueue.trigger('RecruitQueue/start2', [disableNotif])
    }
    RecruitQueue.stop = function () {
        running = false
        eventQueue.trigger('RecruitQueue/stop')
    }
    RecruitQueue.stop2 = function () {
        running = false
        eventQueue.trigger('RecruitQueue/stop2')
    }
    RecruitQueue.isInitialized = function isInitialized() {
        return initialized
    }
    RecruitQueue.isRunning = function () {
        return running
    }
    return RecruitQueue
})